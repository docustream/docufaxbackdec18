﻿using System;
using System.Collections.Generic;
using Faxback.Interface;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using FaxbackModel.Implementation.ORM;
using System.Linq.Expressions;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Data.OleDb;
using DocuStream.ProcessingFramework.Utilities;
using System.Data;

namespace FaxbackModel.Implementation
{
    public delegate IEnumerable<PdfAction> CreateActions<T>(T item, DataTable _templates);

    public class DocuStreamPdfWriter : IObservableFaxProcess
    {
        Action<IEnumerable<PdfJob>, Func<PdfJob, bool>> _whisperChangesToFriend;
        Action<IEnumerable<PdfJob>>                       _callback;
        Func<PdfJob, bool>                                _isUnprocessed;
        Func<PdfJob, bool>                                _readyForWhisper;
        IRecordProvider<PdfJob> _recordProvider;
        DirectoryInfo                                     _outputDirectory;
        private bool _continue;
        private DataTable _templates;

        public DocuStreamPdfWriter(string outputDirectory, Action<IEnumerable<PdfJob>, Func<PdfJob, bool>> whisperChannel, string validationDatabase, IRecordProvider<PdfJob> recordProvider)
        {
            // this is a method for passing new objects between PdfWriter and FaxDispatcher without
            // using a database hit.
            this._whisperChangesToFriend = whisperChannel;
            this._outputDirectory = new DirectoryInfo(outputDirectory);
            this._isUnprocessed = (pdfjob => pdfjob.Startable && (pdfjob.OutputPath == null) && !pdfjob.DoNotSend);
            this._readyForWhisper = (pdfjob => pdfjob.OutputPath != null);
            this._recordProvider = recordProvider;
            LoadPdfTemplate(validationDatabase);
        }
        
        void LoadPdfTemplate(string validationDatabase)
        {
            OleDbConnection connection = new OleDbConnection(UtilityFunctions.CreateOLEConnectionString(validationDatabase));
            connection.Open();
                    var dataTable = new DataTable();
            new OleDbDataAdapter("SELECT FaxbackCode, FaxFilePath FROM DocuFaxback_P2", connection).Fill(dataTable);

            // the important call in this function
            this._templates = dataTable;
        }
        
        public CreateActions<PdfJob> CreateActions { get; set; }
        public bool IsRunning                      { get; set; }
        public IReporter Reporter                  { get; set; }

        public void EnableProcess() => this._continue = true;

        public void Execute()
        {
            this.Reporter.Update(("RUNNING", ""));
            // TODO replace
            //this._buffer.Refresh(_isUnprocessed);
            IEnumerable<PdfJob> unprocessedJobs = this._recordProvider.GetRecords(_isUnprocessed);
            if (!unprocessedJobs.Any())
            {
                this.Reporter.Update(("IDENTIFIER", "None"));
            }
            foreach(var nextJob in unprocessedJobs)
            {
                if (_continue == false)
                {
                    break;
                }
                this.Reporter.Update(("IDENTIFIER", "None"), ("STATUS", "Looking for PDFs to generate..."));
                this.Reporter.Update(("IDENTIFIER", nextJob.Identifier));
                string dateFmt = nextJob.FaxRecDate.Substring(0, 12);
                // output path will be empty, must be assigned.
                nextJob.OutputPath = Path.Combine(
                    _outputDirectory.FullName,
                    $"{nextJob.FaxNumber}_{dateFmt}_{nextJob.Identifier.Split('.')[0]}.pdf"
                );

                try
                {
                    // iterate over actions to perform on PDF, and tell feedback reporter what is happening.
                    foreach (var action in CreateActions(nextJob, _templates))
                    {
                        this.Reporter.Update(("STATUS", action.ActionDescription));
                        action.Execute();
                    }
                    _whisperChangesToFriend(new List<PdfJob> { nextJob }, _readyForWhisper);
                }
                catch (NullInputPathException nipe)
                {
                    this.Reporter.Update(("LOG", $"WARNING!: PdfCreator encountered a missing file path for the specified FaxbackCode for image {nextJob.Identifier}. Setting to DoNotSend=true and skipping."));
                    nextJob.DoNotSend = true;
                    if (this._recordProvider.InsertOrUpdateRecord(nextJob) == false)
                    {
                        this.Reporter.Update(("ERROR", $"Failed to update table for record {nextJob.Identifier}."));
                        // don't worry, this will not pass onto the following catch(Exception e) handler! we're good!
                        throw new Exception("Database failed to update");
                    }
                }
                catch (Exception e)
                {
                    this.Reporter.Update(("ERROR", e.Message)); // datetime.now acts as a timestap.
                    nextJob.DoNotSend = true;
                    this._recordProvider.InsertOrUpdateRecord(nextJob);
                }

                this.Reporter.Update(("STATUS", "Completed PDF annotations."));
            }
            this.Reporter.Update(("STOPPED", ""));
        }

        public void InterruptProcess()
        {
            this.Reporter.Update(("STATUS", "Stopping..."));
            this._continue = false;
        }
        
    }

    /// <summary>
    /// Simple interface for actions we wish to perform on a Pdf.  Based on Action.
    /// </summary>
    public abstract class PdfAction
    {
        public string         OutputPath { get; set; }
        public virtual string ActionDescription { get; }
        public abstract void  CustomExecute(PdfDocument document);
        public abstract void  Execute();
    }

    public abstract class TemplateAnnotationPdfAction : PdfAction
    {
        public string        InputPath { get; set; }
        public override void Execute()
        {
            if (InputPath == null)
            {
                throw new NullInputPathException();
            }
            try
            {
                using (PdfDocument document = PdfReader.Open(InputPath, PdfDocumentOpenMode.Modify))
                {
                    CustomExecute(document);
                    if (!Directory.Exists(new FileInfo(OutputPath).DirectoryName))
                    {
                        Directory.CreateDirectory(new FileInfo(OutputPath).DirectoryName);
                    }
                    document.Save(OutputPath);
                }
            }
            catch (Exception e)
            {
                // attempt to delete the output file if it exists, then throw.
                if (File.Exists(OutputPath))
                {
                    File.Delete(OutputPath);
                }
                throw e;
            }
        }
    }

    [Serializable]
    internal class NullInputPathException : Exception
    {
        public NullInputPathException()
        {
        }

        public NullInputPathException(string message) : base(message)
        {
        }

        public NullInputPathException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NullInputPathException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class TextualAnnotationPdfAction : TemplateAnnotationPdfAction
    {
        public int      FontSize   { get; set; }
        public string   Message    { get; set; }
        public XRect?   Location   { get; set; }
        public override string ActionDescription { get => $"Annotating string: \"{Message}\" at ({Location.Value.X}, {Location.Value.Y})"; }

        public override void CustomExecute(PdfDocument document)
        {
            using (XGraphics gfx = XGraphics.FromPdfPage(document.Pages[0]))
            {
                gfx.DrawString(
                    Message,
                    new XFont("Verdana", FontSize, XFontStyle.Bold),
                    XBrushes.Black,
                    Location.Value,
                    XStringFormats.Center
                );
                gfx.Save();
            }
        }
    }

    public class ImageAnnotationPdfAction : TemplateAnnotationPdfAction
    {
        public string   ChildImagePath  { get; set; }
        public double   X               { get; set; }
        public double   Y               { get; set; }
        public double   Height          { get; set; }
        public double   Width           { get; set; }
        public override string ActionDescription { get => $"Annotating image {new FileInfo(ChildImagePath).Name} at ({X}, {Y})"; }
        public override void CustomExecute(PdfDocument document)
        {
            using (XImage image = XImage.FromFile(ChildImagePath))
            {
                using (XGraphics gfx = XGraphics.FromPdfPage(document.Pages[0]))
                {
                    PdfPage page = document.Pages[0];
                    gfx.DrawImage
                    (
                        image, 
                        page.Width * X, 
                        page.Height * Y, 
                        image.PointWidth * Width, 
                        image.PointHeight * Height
                    );
                }
            }
        }
    }
}
