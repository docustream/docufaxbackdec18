﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faxback.Interface;

namespace FaxbackModel.Implementation.Logging
{
    ///// <summary>
    ///// A generic routing channel solution for connecting messages from observable subjects, to observing entities, such as user interfaces
    ///// and log files. Uses a simple map(string => string) to organize data/messages. requires hand shaking, so a little bit coupled in that regard.
    ///// </summary>
    //public class Reporter : IReporter
    //{
    //    public Reporter()
    //    {
    //        this._observers = new List<IObserver<Message[]>>();
    //        this._state = new Message[] { };
    //    }
    //    private List<IObserver<Message[]>> _observers;
    //    private Message[] _state;
        
    //    public IDisposable Subscribe(IObserver<Message[]> observer)
    //    {
    //        this._observers.Add(observer);
    //        return new Unsubscriber<Message[]>(this._observers, observer);
    //    }

    //    /// <summary>
    //    /// Tells the observers about the subject's new state.
    //    /// </summary>
    //    /// <param name="pairs"></param>
    //    public void Update(params Message[] messages)
    //    {
    //        _state = messages;
    //        _observers.ForEach(obs => obs.OnNext(_state));
    //    }
    //}
}
