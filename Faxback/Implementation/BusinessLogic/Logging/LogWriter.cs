﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Faxback.Interface;

namespace FaxbackModel.Implementation.Logging
{
    public class LogWriter : IObserver<Message>
    {
        private Dictionary<string, string> _lastState;
        private string _logFilePath;

        public LogWriter(string logFilePath)
        {
            this._logFilePath = logFilePath;
            this._lastState = new Dictionary<string, string>();
        }

        public void OnNext(Message message)
        {
            VerifyFile();
            try
            {
                using (var _writer = File.AppendText(this._logFilePath))
                {
                    string outputString = $"{DateTime.Now.ToString()}: ";
                    if (message.Name == "ERROR")
                        outputString += "ERROR! ";
                    outputString += $"{message.Content}";
                    _writer.WriteLine(outputString);
                }
            }
            catch { }
        }

        private void VerifyFile()
        {
            if (!File.Exists(this._logFilePath))
            {
                Directory.CreateDirectory(new FileInfo(this._logFilePath).DirectoryName);
                using (File.Create(this._logFilePath)) { }
            }
        }

        #region unused
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }
#endregion unused
    }
}