﻿using System;
using System.Collections;
using System.Net.Sockets;
using System.IO;
using System.Web.Mail;

namespace dnUtil
{
    public class UtilEMail
    {
        public UtilEMail()
        {
        }

        public static void SendDocuStreamEmail(string strToEmail, string strSubject, string strHTMLBody)
        { SendDocuStreamEmail(strToEmail, strSubject, strHTMLBody, new ArrayList()); }
        public static void SendDocuStreamEmail(string strToEmail, string strSubject, string strHTMLBody, ArrayList arrAttachments)
        {
            SendEmailWithAuthentication("smtp.gmail.com", strToEmail, "general@docustream.com", "d0cuMaiL", strSubject, strHTMLBody, arrAttachments);
        }

        public static void SendEmailWithAuthentication(string strServer, string strToEmail, string strFromEmail, string strPassword,
            string strSubject, string strHTMLBody)
        { SendEmailWithAuthentication(strServer, strToEmail, strFromEmail, strPassword, strSubject, strHTMLBody, new ArrayList()); }
        public static void SendEmailWithAuthentication(string strServer, string strToEmail, string strFromEmail, string strPassword,
            string strSubject, string strHTMLBody, ArrayList arrAttachments)
        {
            MailMessage mail = new MailMessage();
            mail.BodyFormat = System.Web.Mail.MailFormat.Html;
            mail.To = strToEmail;
            mail.Body = strHTMLBody;
            mail.From = strFromEmail;
            mail.Subject = strSubject;

            foreach (string strAttachment in arrAttachments)
            {
                // for binary attachments, base64 encoding is newer and more commonly used than UUEncode?
                MailAttachment attachment = new MailAttachment(strAttachment, MailEncoding.Base64);
                mail.Attachments.Add(attachment);
            }

            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", strServer);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 465);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", strFromEmail);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", strPassword);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "True");
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout", 300);

            SmtpMail.SmtpServer = strServer;
            //SmtpMail.SmtpServer.Insert(0, "smtp.gmail.com")
            SmtpMail.Send(mail);
        }

    }
}