﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FaxbackModel.Implementation.Logging
{
    public class Unsubscriber<TObservered> : IDisposable
    {
        private List<List<IObserver<TObservered>>> _channels;
        private IObserver<TObservered> _participant;

        public Unsubscriber(IEnumerable<List<IObserver<TObservered>>> observers, IObserver<TObservered> observer)
        {
            this._channels = observers.ToList();
            this._participant = observer;
        }

        public void Dispose()
        {
            this._channels.ForEach(channel =>
            {
                if (channel.Contains(_participant))
                    channel.Remove(_participant);
            });
        }
    }
}
