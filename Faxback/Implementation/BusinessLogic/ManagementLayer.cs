﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Faxback.Interface;

namespace FaxbackModel.Implementation.BusinessLogic
{
    /// <summary>
    /// A manager of asynchronous tasks.
    /// 
    /// </summary>
    public class ManagementLayer
    {
        object __lock__actions   = new object();

        // rand generator for thread sleeps. helps avoid lock-race cycling states
        Random random = new Random();

        bool _continue;
        int _maxBackgroundWorkers;
        int _available;
        Queue<Action> _actions;
        List<Action> _interruptions;
        List<IObservableFaxProcess> _processes;

        public ManagementLayer(int maxBackgroundWorkers)
        {
            this._actions   = new Queue<Action>();
            this._interruptions = new List<Action>();
            this._processes = new List <IObservableFaxProcess>();
            this._maxBackgroundWorkers = maxBackgroundWorkers;
            this._available = this._maxBackgroundWorkers;
        }

        public void AddProcess(IObservableFaxProcess process)
        {
            lock (__lock__actions)
            {
                _interruptions.Add(process.InterruptProcess);
                _processes.Add(process);
                _actions.Enqueue(process.Execute);
            }
        }

        public void AddActions(List<IObservableFaxProcess> tasks)
        {
            lock (__lock__actions)
            {
                tasks.Select<IObservableFaxProcess, Action>(t => t.Execute).ToList().ForEach(_actions.Enqueue);
                _processes.AddRange(tasks);
            }
        }

        public void RunTasks()
        {
            // ensure layer isn't already managing processes.
            if (_continue == false)
            {
                // kick off child process which will run our processes
                Task.Run(() =>
                {
                    _continue = true;
                    // this loop can only be interrupted by the Toggle() function.
                    while (_continue)
                    {
                        // make sure we have exclusive control over the available count variable.
                        if (Monitor.TryEnter(__lock__actions))
                        {
                            try
                            {
                                if (_available > 0)
                                {
                                    if (this._actions.Count > 0)
                                    {
                                        _available--;
                                        var action = this._actions.Dequeue();
                                        if (action != null)
                                        {
                                            Task.Run(action)
                                                .ContinueWith(t => Requeue(action));
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                Monitor.Exit(__lock__actions);
                            }
                        }
                        else
                        {
                            Thread.Sleep(random.Next(100, 1000));
                        }
                    }
                });
            }
        }

        void Requeue(Action action)
        {
            bool requeued = false;
            while (!requeued)
            {
                lock(__lock__actions)
                {
                    this._actions.Enqueue(action);
                    _available++;
                    requeued = true;
                }
            }
        }

        public void StopProcesses()
        {
            this._processes.ForEach(p => 
            p.InterruptProcess());
            _continue = false;
        }

        public void WaitAll()
        {
            var allProcessesAtRest = false;
            while(!allProcessesAtRest)
            {
                lock(__lock__actions)
                {
                    allProcessesAtRest = _available == _maxBackgroundWorkers;
                }
                if (!allProcessesAtRest)
                {
                    Thread.Sleep(random.Next(1000, 4000));
                }
            }
        }
    }
}
