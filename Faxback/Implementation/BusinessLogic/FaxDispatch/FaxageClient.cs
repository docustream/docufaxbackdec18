﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Faxback.Implementation.ORM;
using Faxback.Interface;

namespace Faxback.Implementation.Faxage
{
    /// <summary>
    /// Faxage API service specific implementation of IFaxClient
    /// </summary>
    public class FaxageLiteClient : IFaxClient<Fax>
    {
        /// <summary>
        /// ioc configuration object for sending messages to the faxage api.
        /// </summary>
        private FaxageLiteClientConfiguration _configuration;

        /// <summary>
        /// A strategy for resolving the fax number to be used. allows top-level composability of the FaxageLiteClient.
        /// </summary>
        private Func<Fax, string> _resolveFaxNumber;
        /// <summary>
        /// constructor. takes a configuration
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="resolveFaxNumber">Optional alternative strategy for resolving fax #.</param>
        public FaxageLiteClient(FaxageLiteClientConfiguration configuration, Func<Fax,string> resolveFaxNumber = null)
        {
            if (!configuration.IsValid())
            {
                throw new FormatException($"{configuration} is not an acceptable FaxageLiteClientConfiguration object.");
            }
            else
            {
                this._configuration = configuration;
                this._resolveFaxNumber = (resolveFaxNumber == null) ? 
                        // default to just returning the fax number,
                        (f) => f.FaxNumber :  
                        // OR use a user-defined resolve-strategy.
                        resolveFaxNumber;
            }
        }

        public void Send(Fax fax)
        {
            var responseJobId = 
                Encoding.ASCII.GetString(
                    new WebClient()
                        .UploadValues(
                            @"https://api.faxage.com/httpsfax.php", 
                            ConstructFaxageAPISendFaxMessage(fax)
                        )
                );
            if (responseJobId.StartsWith("JOBID:", StringComparison.Ordinal))
            {
                fax.Job.JobId = responseJobId.Substring(7).TrimEnd('\n');
                fax.Job.Status = "pending";
                fax.Job.Unsent = false;
                fax.Job.Unfinished = true;
                fax.FaxBackTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else if (responseJobId.StartsWith("ERR", StringComparison.Ordinal))
            {
                fax.Job.JobId = responseJobId;
                fax.Job.Status = "failure";
                fax.Job.Unsent = false;
                fax.Job.Ignore = responseJobId == "DoNotSend" || responseJobId == "UnknownFaxNumber";
            }
            else
            {
                throw new FormatException($"{nameof(FaxageLiteClient)} threw an exception in {nameof(Send)}() " +
                    $"because Faxage API response '{responseJobId}' was unexpected.");
            }
        }

        public void UpdateStatus(Fax fax)
        {
            var faxFileBytes_64 = Convert.ToBase64String(File.ReadAllBytes(fax.FilePath));
            var RESP = new WebClient().UploadValues(@"https://api.faxage.com/httpsfax.php", ConstructFaxageAPIStatusMessage(fax));
            var responseStatus = Encoding.ASCII.GetString(RESP);
            fax.Job.Status = responseStatus.Split('\t')[4];
            fax.Job.Response = responseStatus.Replace('\t', '\n');
        }

        /// <summary>
        /// creates message body for sendfax 
        /// api command
        /// </summary>
        /// <param name="fax"></param>
        /// <returns></returns>
        NameValueCollection ConstructFaxageAPISendFaxMessage(Fax fax)
        {
            string faxfiledata = null;
            if (!TryReadAllBytes(fax, ref faxfiledata))
            {
                throw new FileNotFoundException($"ConstructFaxageAPISendFaxMessage(fax): The file could not be accessed. \nCurrentFax: \n{fax.ToString()}");
            }
            return ConstructFaxageAPIMessage(
                fax,
                "sendfax",
                ("faxno", this._resolveFaxNumber(fax)),
                ("faxfilenames[0]", fax.FilePath),
                ("faxfiledata[0]", faxfiledata)
            );
        }

        private static bool TryReadAllBytes(Fax fax, ref string faxfiledata)
        {
            int fileAccessTimeoutAttempts = 0, fileAccessTimeoutAttemptsMax = 10;
            bool success = false;
            while (fileAccessTimeoutAttempts < fileAccessTimeoutAttemptsMax && !success)
            {
                try
                {
                    faxfiledata = Convert.ToBase64String(File.ReadAllBytes(fax.FilePath));
                    success = true;
                }
                catch
                {
                    Thread.Sleep(1000);
                    fileAccessTimeoutAttempts++;
                }

            }
            return success;
        }

        /// <summary>
        /// creates message body for status faxage api command
        /// </summary>
        /// <param name="fax"></param>
        /// <returns></returns>
        NameValueCollection ConstructFaxageAPIStatusMessage(Fax fax) =>
            ConstructFaxageAPIMessage(
                fax,
                "status",
                ("jobid", fax.Job.JobId)
            );

        /// <summary>
        /// creates generic faxage api command.
        /// </summary>
        /// <param name="fax"></param>
        /// <param name="cmd"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        NameValueCollection ConstructFaxageAPIMessage(Fax fax, string cmd, params (string key, string value)[] parameters)
        {

            //post the data
            NameValueCollection NC = new NameValueCollection();
            NC.Add("username", _configuration.Username);
            NC.Add("company", _configuration.CompanyIdentifier);
            NC.Add("password", _configuration.Password);
            NC.Add("operation", cmd);
            foreach(var pair in parameters)
            {
                NC.Add(pair.key, pair.value);
            }
            //NC.Add("faxno", fax.FaxNumber);
            //NC.Add("faxfilenames[0]", fax.FilePath);
            //NC.Add("faxfiledata[0]", encodedBase64);
            return NC;
        }

    }

    /// <summary>
    /// configures user-credentials for lite faxage client.
    /// </summary>
    public struct FaxageLiteClientConfiguration
    {
        public string Username          { get; set; }
        public string CompanyIdentifier { get; set; }
        public string Password          { get; set; }

        public bool IsValid() => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(CompanyIdentifier) && !string.IsNullOrEmpty(Password);

        public override string ToString() => "{ "+ $"Username: {Username}, Id: {CompanyIdentifier}, Password: {Password}" + " }";
    }
}
