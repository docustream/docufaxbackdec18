﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Utilities;
using Faxback.Implementation.ORM;
using Faxback.Interface;
using FaxbackModel.Implementation;

// To fully decouple:
// 1. Make isUn- expressions object inputs.
//

namespace Faxback.Implementation
{
    /// <summary>
    /// An implementation of a strategy for dispatching faxes in serial order from a stream of faxes.
    /// </summary>
    public class SimpleFaxageFaxDispatcher : IObservableFaxProcess
    {
        /// <summary>
        /// A Faxage API client.
        /// </summary>
        IFaxClient<Fax> _client;
        IRecordProvider<Fax> _provider;
        public object @lock = new object();
        public HashSet<Fax> Faxes
        {
            get;set;
        }

        /// <summary>
        /// Whether this dispatcher should continue dispatching new faxes, if possible.
        /// Since Booleans are atomic in C#, we don't need any kind of lock on this.
        /// </summary>
        internal enum FaxageState
        {
            Pending,
            Success,
            Failure
        }

        Timer _timer;
        bool _getUpdateTimerHasElapsed;

        public bool IsRunning { get; set; }
        public IReporter Reporter { get; set; }

        public Action _send;
        public Action _update;
        private bool _continue;

        private bool disableOperatingThreadSleep;

        public SimpleFaxageFaxDispatcher(IFaxClient<Fax> client, IRecordProvider<Fax> provider, bool disableOperatingThreadSleep = false, int hardUpdateStartIntervalMilliseconds = 300000, int hardUpdateRepeatIntervalMilliseconds = 900000) 
        {
            this._client = client;
            this._provider = provider;
            Func<Fax, bool> isUnsent = fax => fax.Sendable && !fax.DoNotSend && fax.Job.Unsent && fax.FilePath != null;
            Func<Fax, bool> isUnfinished = fax => !fax.Job.Ignore && fax.Sendable && !fax.DoNotSend && fax.Job.Unfinished && fax.FilePath != null && fax.Job.JobId != null && fax.Job.Status.ToUpper() != "SUCCESS" && fax.Job.Status.ToUpper() != "FAILURE";  // 2.0.1.9
            Func<Fax, bool> isUnfinishedOrUnsent = fax => isUnsent(fax) || isUnfinished(fax);
            //this._buffer.Refresh(isUnsent);
            //this._buffer.Refresh(isUnfinished);

            this.disableOperatingThreadSleep = disableOperatingThreadSleep;

            this.Faxes = this._provider.GetRecords(isUnfinishedOrUnsent).ToHashSet();

            _send = new Action(() =>
            {
                Reporter.Update(("IDENTIFIER", "None"));
                
                bool dontRun;
                lock(@lock) dontRun = !Faxes.Any(isUnsent);
                if (dontRun)
                {
                    bool reallyDontRun;
                    lock(@lock) reallyDontRun = !Faxes.Any(isUnfinished);

                    if (reallyDontRun)
                        if (!disableOperatingThreadSleep)
                            Thread.Sleep(10 * 1000);
                    else
                        _update();

                }
                else
                {
                    SendFaxes(isUnsent);
                }
            });

            _update = new Action(() =>
            {
                // note that we do not update Faxes in anyway here,
                // we just check if any records have been updated.
                // this is because the Pdf thread will whisper new Faxjobs to this process.
                // we just need to hodl the lock to ensure that a Fax update does not get snuck in behind us.
                IEnumerable<Fax> unfinishedFaxes = null;
                
                lock(@lock) unfinishedFaxes = Faxes.Where(isUnfinished).Take(25).ToList();
                    
                if (!unfinishedFaxes.Any())
                {
                    if(!disableOperatingThreadSleep)
                        Thread.Sleep(10 * 1000);
                }
                else
                {
                    UpdateFaxes(isUnfinished, unfinishedFaxes);
                }
            });

            _timer = new Timer(obj =>
            {
                _getUpdateTimerHasElapsed = true;
            }, null, hardUpdateStartIntervalMilliseconds, hardUpdateRepeatIntervalMilliseconds);
        }

        /// <summary>
        ///  we want the send and update tasks to run in parallel, if they need to run.
        /// </summary>
        public void Execute()
        {
            Reporter.Update(("RUNNING", ""));
            if (this._continue == false)
                this._continue = true;
            var tasks = new Task[1];
            
            if (_getUpdateTimerHasElapsed)
            {
                tasks[0] = Task.Run(_update);
                _getUpdateTimerHasElapsed = false;
            }
            else
            {
                tasks[0] = Task.Run(_send);
            }

            try
            {
                Task.WaitAll(tasks.Where(t => t != null).ToArray());
            }
            catch (Exception e)
            {
                // what the fuck?
                var x = 0;
            }
            Reporter.Update(("STOPPED", ""));
        }

        private void SendFaxes(Func<Fax, bool> isUnsent)
        {
            // dispatch as many faxes as the stream will allow
            // TODO replace
            IEnumerable<Fax> sendableFaxes = null;
            lock (@lock) sendableFaxes = Faxes.Where(isUnsent);
          
            List<Fax> processedFaxes = new List<Fax>();
            foreach (var nextFax in sendableFaxes.ToHashSet())
            {
                if (this._continue == false)
                {
                    break;
                }

                Reporter.Update(
                    ("IDENTIFIER", $"{nextFax.Identifier}"),
                    ("STATUS", $"Found PDF at path '{nextFax.FilePath}' for Image '{nextFax.Identifier}'. Faxing...")
                );
                try
                {
                        this._client.Send(nextFax);
                        processedFaxes.Add(nextFax);
                }
                catch (Exception e)
                {
                    nextFax.Job.JobId = "Fail";
                    nextFax.FaxBackTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.ttt");
                    Reporter.Update(("ERROR", $"Exception encountered during attempt to send fax {nextFax.FilePath}: \n{e.Message}"));
                }
                if (int.TryParse(nextFax.Job.JobId, out int jobId))
                {
                    Reporter.Update(("STATUS", $"Successfully delivered to faxage. JobId = {jobId}"));
#if DEBUG
                    Reporter.Update(("DBGLOG", $"[ImageName] = '{nextFax.Identifier}' OR"));
#endif
                }
                else
                {
                    Reporter.Update(("STATUS", $"Failed to deliver fax to faxage."));
                }
            }
            this._provider.InsertOrUpdateRecords(processedFaxes);
        }

        private void UpdateFaxes(Func<Fax, bool> isUnfinished, IEnumerable<Fax> unfinishedFaxes)
        {
                Reporter.Update(("STATUS", $"Polling for deilvered faxes..."));
                List<Fax> updatedFaxes = new List<Fax>();
                foreach (var fax in unfinishedFaxes)
                {
                    if (_continue == false)
                    {
                        break;
                    }
                    _client.UpdateStatus(fax);
                    if (fax.Job.Status == FaxJob.COMPLETE)
                    {
                        Reporter.Update(("LOG", $"\n*===status response from faxage===*\n{fax.Job.Response}*=================================*"));
                        Reporter.Update(("STATUS", $"PDF '{new FileInfo(fax.FilePath).Name}' for Image '{fax.Identifier}' successfully delivered to client."));
                        if (_provider.UpdateFaxStatus(fax.Identifier, fax.Job.Status, fax.Job.JobId)) // 2.0.1.8 update FaxStatus right away
                        {
                            Reporter.Update(("STATUS", $"Updated FaxBackTracking FaxStatus to '{FaxJob.COMPLETE}' for Image '{fax.Identifier}' with JobID {fax.Job.JobId}\n"));
                        }
                        // updatedFaxes.Add(fax);
                    }
                    else if (fax.Job.Status == FaxJob.FAILURE)
                    {
                        Reporter.Update(("LOG", $"\n*===status response from faxage===*\n{fax.Job.Response}*=================================*"));
                        Reporter.Update(("STATUS", $"PDF '{new FileInfo(fax.FilePath).Name}' for Image '{fax.Identifier}' failed to fax to client via faxage.\n"));
                        if (_provider.UpdateFaxStatus(fax.Identifier, fax.Job.Status, fax.Job.JobId)) // 2.0.1.8 update FaxStatus right away
                        {
                            Reporter.Update(("STATUS", $"Updated FaxBackTracking FaxStatus to '{FaxJob.FAILURE}' for Image '{fax.Identifier}' with JobID {fax.Job.JobId}\n"));
                        }
                        //updatedFaxes.Add(fax);
                    }
                }
                //_provider.InsertOrUpdateRecords(updatedFaxes);
        }

        public void InterruptProcess()
        {
            this._continue = false; Reporter.Update(("STATUS", "Stopping..."));
        }

        public void EnableProcess() => this._continue = true;
    }

    // credit to: http://www.extensionmethod.net/1962/csharp/ienumerable-t/tohashset-t
    public static class LinqExtension
    {
        /// <summary>
        /// Converts an IEnumerable to a HashSet
        /// </summary>
        /// <typeparam name="T">The IEnumerable type</typeparam>
        /// <param name="enumerable">The IEnumerable</param>
        /// <returns>A new HashSet</returns>
        // I am Groot. -- Groot
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable is HashSet<T>)
            {
                return enumerable as HashSet<T>;
            }
            HashSet<T> hs = new HashSet<T>();
            foreach (T item in enumerable)
                hs.Add(item);
            return hs;
        }
    }

}