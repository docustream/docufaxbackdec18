﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Utilities;
using Faxback.Implementation.ORM;

namespace FaxbackModel.Implementation.DAL
{
    public class FaxRecordProvider : IRecordProvider<Fax>
    {
        private IEnvironmentState state;
        SqlConnection connection;
        HashSet<Fax> faxes;
        public FaxRecordProvider(IEnvironmentState state)
        {
            this.state = state;
            connection = new SqlConnection(UtilityFunctions.CreateSQLConnectionString(state, "TrackingDBName"));
        }

        public IEnumerable<Fax> GetRecords(Func<Fax, bool> predicate = null)
        {
            if (faxes == null)
                faxes = new HashSet<Fax>();

            try
            {
                string commandBody =
                    $"SELECT\n" +
                    $"  [{state["TrackingDBTable"]}].[ImageName],\n" +
                    $"  [{state["TrackingDBTable"]}].[FaxNumber],\n" +
                    $"  [{state["TrackingDBTable"]}].[OutFaxFile],\n" +
                    $"  [{state["TrackingDBTable"]}].[DoNotSend],\n" +
                    $"  [{state["TrackingDBTable"]}].[FaxBackTime],\n" +
                    $"  [{state["TrackingDBTable"]}].[FaxJobID],\n" +
                    $"  [{state["TrackingDBTable"]}].[FaxStatus]\n" +
                    (
                        (state["UploadCompletedDate"] == "1") ?
                            $",[{state["WFTrackingTable"]}].[CompletedDate] FROM [{state["TrackingDBTable"]}] JOIN [{state["WFTrackingTable"]}] on [{state["WFTrackingTable"]}].[ImageName] = [{state["TrackingDBTable"]}].[ImageName] \n" :
                            $"FROM [{state["TrackingDBTable"]}]"
                    ) +
                    $" WHERE \n" +
                    (
                        (state["UploadCompletedDate"] == "1") ?
                            $"[{state["WFTrackingTable"]}].[CompletedDate] IS NOT NULL AND \n" :
                            ""
                    ) +
                    $"[{state["TrackingDBTable"]}].[DoNotSend] = 0 AND \n" +
                    $"[{state["TrackingDBTable"]}].[OutFaxFile] IS NOT NULL AND \n" +
                    $"(\n" +
                    $"  (\n" +
                    $"      ([{state["TrackingDBTable"]}].[FaxStatus] IS NULL OR ([{state["TrackingDBTable"]}].[FaxStatus] != 'success' AND [{state["TrackingDBTable"]}].[FaxStatus] != 'failure')) AND\n" +
                    $"      [{state["TrackingDBTable"]}].[FaxJobID] IS NOT NULL AND\n" +
                    $"      ([{state["TrackingDBTable"]}].[FaxJobID] != 'UnknownFaxName' AND [{state["TrackingDBTable"]}].[FaxJobID] != 'DoNotSend')\n" +
                    $"  )\n" +
                    $"  OR\n" +
                    $"  (\n" +
                    $"      [{state["TrackingDBTable"]}].[FaxStatus] IS NULL\n AND" +
                    $"      [{state["TrackingDBTable"]}].[FaxJobID] IS NULL" +
                    $"  )\n" +
                    $")\n";

                DataSet set = new DataSet();

                SqlDataAdapter adp = new SqlDataAdapter(commandBody, connection);
                connection.Open();
                adp.Fill(set, "Q1");
                foreach (DataRow row in set.Tables["Q1"].Rows)
                {
                    string faxJobId = row["FaxJobID"] as string;
                    string status = row["FaxStatus"] as string;
                    faxes.Add(new Fax
                    {
                        DoNotSend = (bool)row["DoNotSend"],
                        FaxBackTime = row["FaxBackTime"] as string,
                        FaxNumber = row["FaxNumber"] as string,
                        FilePath = row["OutFaxFile"] as string,
                        Identifier = row["ImageName"] as string,
                        Job = new FaxJob
                        {
                            Ignore = faxJobId == "UnknownFaxNumber" || faxJobId == "DoNotSend",
                            JobId = faxJobId,
                            Response = null,
                            Status = status,
                            Unsent = status == null && faxJobId == null,
                            Unfinished = faxJobId != null && (status == null || (status != "success" && status != "failure"))
                        },
                        Sendable = state["UploadCompletedDate"] == "1" ? row["CompletedDate"] != null : true
                    });
                }

                if (predicate != null)
                    return faxes.Where(predicate);

                return faxes;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }
        }

        public bool InsertOrUpdateRecord(Fax record)
        {
            throw new NotImplementedException();
        }

        public bool InsertOrUpdateRecords(IEnumerable<Fax> records)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                foreach (var record in records)
                {
                    string command = $"UPDATE [{state["TrackingDBTable"]}] SET " +
                        $"[{state["TrackingDBTable"]}].[OutFaxFile] = '{record.FilePath}', " +
                        $"[{state["TrackingDBTable"]}].[FaxJobID] = '{record.Job.JobId}', " +
                        $"[{state["TrackingDBTable"]}].[FaxStatus] = '{record.Job.Status}', " +
                        $"[{state["TrackingDBTable"]}].[FaxBackTime] = '{record.FaxBackTime}' " +
                        $"WHERE [{state["TrackingDBTable"]}].[ImageName] = '{record.Identifier}'";
                    using (var cmd = new SqlCommand(command, connection))
                    {
                        var count = cmd.ExecuteNonQuery();
                        if(count < 1)
                        {
                            var x = 0;
                        }
                    }
                }

                return true;
            }
            catch
            {
                return false;            
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

        }

        public bool UpdateFaxStatus(string imageName, string status, string jobID) 
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                string command = $"UPDATE [{state["TrackingDBTable"]}] SET " +
                $"[{state["TrackingDBTable"]}].[FaxStatus] = '{status}' " +
                $"WHERE [{state["TrackingDBTable"]}].[ImageName] = '{imageName}' " +
                $"AND [{ state["TrackingDBTable"]}].[FaxJobID] = '{jobID}'";
                using (var cmd = new SqlCommand(command, connection))
                {
                    var count = cmd.ExecuteNonQuery();
                    if (count < 1)
                    {
                        var x = 0;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }
        }
    }
}
