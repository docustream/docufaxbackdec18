﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuStream.ProcessingFramework.Utilities;
using FaxbackModel.Implementation.ORM;

namespace FaxbackModel.Implementation.DAL
{
    public class PdfRecordProvider : IRecordProvider<PdfJob>
    {
        IEnvironmentState state;
        SqlConnection connection;
        HashSet<PdfJob> records;
        public PdfRecordProvider(IEnvironmentState state)
        {
            this.state = state;
            connection = new SqlConnection(UtilityFunctions.CreateSQLConnectionString(state, "TrackingDBName"));
            records = new HashSet<PdfJob>();
        }

        public IEnumerable<PdfJob> GetRecords(Func<PdfJob, bool> predicate = null)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                SqlDataAdapter adp = null;
                DataSet set = new DataSet();
                if (state["UploadCompletedDate"] == "1")
                {
                    // TODO: use TOP n?
                
                    adp = new SqlDataAdapter(
                        $"SELECT [{state["TrackingDBTable"]}].[ImageName], \n" +
                        $"    [TiffImagePath], \n" +
                        $"    [DoNotSend], \n" +
                        $"    [OutFaxFile], \n" +
                        $"    [FaxReceivePDFDate], \n" +
                        $"    [{state["TrackingDBTable"]}].[FaxBackTime], \n" +
                        $"    [CompletedDate], \n" +
                        $"    [FaxbackCode], \n" +
                        $"    [FaxStatus], \n" +
                        $"    [{state["TrackingDBTable"]}].[FaxNumber] \n" +
                        $"FROM [{ state["TrackingDBName"]}].[dbo].[{state["TrackingDBTable"]}] \n" +
                        $"JOIN [{ state["TrackingDBName"]}].[dbo].[{ state["WFTrackingTable"]}] ON [{state["TrackingDBTable"]}].[ImageName] = [{ state["WFTrackingTable"]}].[ImageName] \n" +
                        $"WHERE [{state["TrackingDBTable"]}].[FaxBackTime] IS NULL AND [CompletedDate] IS NOT NULL AND OutFaxFile IS NULL AND DoNotSend = 0"    
                    , connection);
                }
                else
                {
                    adp = new SqlDataAdapter(
                        $"SELECT [{state["TrackingDBTable"]}].[ImageName], \n" +
                        $"  [TiffImagePath], \n" +
                        $"  [DoNotSend], \n" +
                        $"  [OutFaxFile], \n" +
                        $"  [FaxReceivePDFDate], \n" +
                        $"  [{state["TrackingDBTable"]}].[FaxBackTime], \n" +
                        $"  [FaxbackCode], \n" +
                        $"  [FaxStatus], \n" +
                        $"  [{state["TrackingDBTable"]}].[FaxNumber] \n" +
                        $"FROM [{ state["TrackingDBName"]}].[dbo].[{state["TrackingDBTable"]}] \n" +
                        $"JOIN [{ state["TrackingDBName"]}].[dbo].[{ state["WFTrackingTable"]}] ON [{state["TrackingDBTable"]}].[ImageName] = [{ state["WFTrackingTable"]}].[ImageName] \n" +
                        $"WHERE [{state["TrackingDBTable"]}].[FaxBackTime] IS NULL AND [OutFaxFile] IS NULL AND [DoNotSend] = 0", connection);
                }
                adp.Fill(set, "Q1");
                foreach (DataRow row in set.Tables["Q1"].Rows)
                {
                    records.Add(new PdfJob
                    {
                        Identifier = row["ImageName"] as string,
                        DoNotSend = (bool)row["DoNotSend"],
                        FaxbackCode = row["FaxbackCode"] as string,
                        FaxNumber = row["FaxNumber"] as string,
                        FaxRecDate = row["FaxReceivePDFDate"] as string,
                        OutputPath = row["OutFaxFile"] as string,
                        Startable = (row["FaxBackTime"] is System.DBNull) && (state["UploadCompletedDate"] == "1" && !(row["CompletedDate"] is System.DBNull)) || (state["UploadCompletedDate"] != "1"),
                        Status = row["FaxStatus"] as string,
                        TiffImagePath = row["TiffImagePath"] as string
                    });
                }
            
                if (predicate != null)
                    return records.Where(predicate);
                return records;


            }
            catch (Exception e)
            {
                var x = 0;
                throw;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }
        }

        public bool InsertOrUpdateRecord(PdfJob record)
        {
            // bad design, but let's just make it automatically set DoNotSend to true, and that's all, just to keep the ball a-rollin'
            if (records.Contains(record))
            {
                var cmd = new SqlCommand($"UPDATE [{ state["TrackingDBName"]}].[dbo].[{state["TrackingDBTable"]}] SET DoNotSend = 1 WHERE [ImageName] = '{record.Identifier}'", connection);
                connection.Open();
                var rowsAffected = cmd.ExecuteNonQuery();
                connection.Close();
                return rowsAffected == 1;
            }
            else return false;
        }

        public bool InsertOrUpdateRecords(IEnumerable<PdfJob> records)
        {
            throw new NotImplementedException("PdfProvider doesn't need this yet.");
        }

        public bool UpdateFaxStatus(string imageName, string status, string jobID) 
        {
            throw new NotImplementedException("PdfProvider doesn't need this yet.");
        }
    }
}
