﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using FaxbackModel.Implementation.DAL;

namespace FaxbackModel.Implementation.ORM
{
    
    public class PdfJob : IIdentifiable
    {
        public static Dictionary<int,DataTable> Templates { get; set; }
        public virtual string Identifier  { get; set; }
        public virtual string TiffImagePath { get; set; }
        public virtual bool DoNotSend { get; set; }

        public virtual void Copy(IIdentifiable identifiable)
        {
            if (identifiable is PdfJob other_job)
            {
                this.FaxbackCode = other_job.FaxbackCode;
                this.FaxNumber = other_job.FaxNumber;
                this.FaxRecDate = other_job.FaxRecDate;
                this.OutputPath = other_job.OutputPath;
                this.Status = other_job.Status;
                this.TiffImagePath = other_job.TiffImagePath;
            }
        }

        public virtual string OutputPath        { get; set; }
        public virtual string Annotation {
            get
            {
                // MM/dd/yyyy hh:mm from Fax # XXXXXXXXX
                return
                    $"{FaxRecDate.Substring(4, 2)}/{FaxRecDate.Substring(6, 2)}/{FaxRecDate.Substring(0, 4)} " +
                    $"{FaxRecDate.Substring(8, 2)}:{FaxRecDate.Substring(10, 2)}" +
                    $" from Fax # {FaxNumber}";
            }
        }
        public virtual string FaxRecDate        { get; set; } 
        public virtual string FaxNumber         { get; set; }
        public virtual bool   Startable         { get; set; }
        public virtual string FaxbackCode       { get; set; }
        public virtual string Status            { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is PdfJob other_job)
            {
                return Identifier.Equals(other_job.Identifier);
            }
            return false;
        }
        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }
    }
}
