﻿using FaxbackModel.Implementation.DAL;
using FaxbackModel.Implementation.ORM;

namespace Faxback.Implementation.ORM
{
    public class Fax : IIdentifiable
    {
        public static implicit operator Fax(PdfJob d)
        {
            return new Fax
            {
                DoNotSend = false,
                FaxBackTime = null,
                FaxNumber = d.FaxNumber,
                FilePath = d.OutputPath,
                Identifier = d.Identifier,
                Job = new FaxJob
                {
                    Ignore = false,
                    JobId = null,
                    Response = null,
                    Status = null,
                    Unfinished = true,
                    Unsent = true
                },
                Sendable = true
            };
        }

        public virtual string Identifier  { get; set; }
        public virtual string FaxNumber   { get; set; }
        public virtual string FilePath    { get; set; }
        public virtual bool   Sendable    { get; set; }
        public virtual bool   DoNotSend   { get; set; }
        public virtual FaxJob Job         { get; set; }
        public virtual string FaxBackTime { get; set; }

        public override string ToString() => 
            $"Fax:" +
            $"\tIdentifier  ={Identifier}\n" +
            $"\tFaxNumber   ={FaxNumber}(may be misleading if in test mode)\n" +
            $"\tFilePath    ={FilePath}\n" +
            $"\tSendable    ={Sendable}\n" +
            $"\tDoNotSend   ={DoNotSend}\n" +
            $"\tJob         =({Job.ToString()})\n" +
            $"\tFaxBackTime ={FaxBackTime}";

        public virtual void Copy(IIdentifiable identifiable)
        {
            if (identifiable is Fax other_job)
            {
                this.FaxBackTime = other_job.FaxBackTime;
                this.FaxNumber = other_job.FaxNumber;
                this.FilePath = other_job.FilePath;
                this.Job = other_job.Job;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Fax other_fax)
            {
                return Identifier.Equals(other_fax.Identifier);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Identifier.GetHashCode();
        }
    }
    
    public class FaxJob
    {
        public static readonly string COMPLETE = "success";
        public static readonly string FAILURE  = "failure";

        public virtual string JobId    { get; set; }
        public virtual bool Ignore     { get; set; }
        public virtual bool Unsent     { get; set; }
        public virtual bool Unfinished { get; set; }
        public virtual string Status   { get; set; }
        public string Response         { get; set; }

        public override string ToString()
        {
            return $"Id:{JobId}, Status:{Status}";
        }

        public override bool Equals(object obj)
        {
            if (obj is FaxJob other_job)
            {
                return other_job.JobId == this.JobId && other_job.Status == this.Status;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (this.JobId.GetHashCode() * 31 * 31) + this.Status.GetHashCode();
        }
    }
}
