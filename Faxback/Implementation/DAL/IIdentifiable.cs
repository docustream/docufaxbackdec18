﻿namespace FaxbackModel.Implementation.DAL
{
    public interface IIdentifiable
    {
        string Identifier { get; set; }
        void Copy(IIdentifiable i);
    }
}