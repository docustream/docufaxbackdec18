﻿namespace DocuFaxbackUI
{
    using System;
    using System.Windows;
    using DocuFaxbackUI.Reporting;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
#if (DEBUG == true)
            ConsoleManager.Show();
#endif
            InitializeComponent();
        }
    }
}
