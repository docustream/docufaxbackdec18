﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using DocuFaxbackUI.Bootstraps;
using DocuFaxbackUI.ViewModels;
using DocuStream.ProcessingFramework.Utilities;
using Faxback.Implementation;
using Faxback.Implementation.Faxage;
using Faxback.Implementation.ORM;
using Faxback.Interface;
using FaxbackModel.Implementation;
using FaxbackModel.Implementation.DAL;
using FaxbackModel.Implementation.Logging;
using FaxbackModel.Implementation.ORM;

namespace DocuFaxbackUI
{
    internal class MultiflowBootstrap
    {
        static string[] APPCARDCHANNELS = { "ERROR", "STATUS", "LOG", "IDENTIFIER", "TRACKING", "QUEUE", "QUEUE_MAX", "QUEUE_TYPE", "RUNNING", "STOPPED", 
#if DEBUG
            "DBGLOG"
#endif
        };
        static string[] LOGGERCHANNELS  = { "ERROR", "STATUS", "LOG" };
        static string[] EMAILERCHANNELS = { "ERROR" };
        /// <summary>
        /// A setup function for versions of DocuFaxback dated after v2.0.0.*.
        /// Takes a list of INI parameters and composes either a fleet of pdf-drawing processes or a fleet of fax-sending processes.
        /// </summary>
        /// <param name="states">A list of ini options for supporting multiple INI parameters.</param>
        /// <returns>The view model for the main ui.</returns>
        internal static MainViewModel GetMainViewModel(List<IEnvironmentState> states = null, bool autostart = false, string title = "DocuFaxback", int maxThreads = 10)
        {
            (var views, var processModels) = 
                GetViewsAndModels(
                    title:  title, 
                    states: states
                );

            return new MainViewModel(
                fullAppCards:   views, 
                processes:      processModels, 
                title:          title, 
                maxThreads:     maxThreads,
                autostart:      autostart
            );
        }

        public class FreshFaxComparer : IEqualityComparer<Fax>
        {
            public bool Equals(Fax x, Fax y)
            {
                return x.Identifier == y.Identifier;
            }

            public int GetHashCode(Fax obj)
            {
                return obj.Identifier.GetHashCode();
            }
        };
        static FreshFaxComparer _faxComp = new FreshFaxComparer();

        static (IEnumerable<FullAppCardViewModel> views, IEnumerable<IObservableFaxProcess> processModels) GetViewsAndModels(string title, List<IEnvironmentState> states)
        {
            // create a new FullAppCard for each ini
            return
                states.Select(state =>
                {
                    (var faxView, var faxProcess, var whisper) = Create_FaxbackAppCard_WithProcess(
                                    state:      state,
                                    reporter:   new MultiChannelReporter(APPCARDCHANNELS),
                                    title:      title
                    );
                    
                    (var pdfView, var pdfProcess) = Create_PdfCreatorAppCard_WithProcess(
                                    state:      state,
                                    // each should have it's own MultiChannelReporter, so as to not have log-file/ui overlap
                                    reporter:   new MultiChannelReporter(APPCARDCHANNELS),
                                    title:      title,
                                    whisper:    whisper
                    );
                    
                    return
                    (
                        view:
                            new FullAppCardViewModel(
                              // that FullAppCard should be composed of a PdfCreator AppCard and a Faxback AppCard.
                              pdfCard: pdfView,
                              faxCard: faxView
                            ),
                        processModels:
                            new List<IObservableFaxProcess> { pdfProcess, faxProcess }
                    );
                }).Aggregate(
                    seed: (
                        views:          new List<FullAppCardViewModel> { },
                        processModels:  new List<IObservableFaxProcess> { }
                    ),
                    // This is just an ugly little piece of code to compile a tuple of the list of views and list of models we just created.
                    func: (accumulator, unit) =>
                    {
                        List<FullAppCardViewModel> list = accumulator.views.ToList();
                        list.Add(unit.view);
                        return 
                        (
                            views:          list,
                            processModels:  accumulator.processModels.Union(unit.processModels).ToList()
                        );
                    }
                );
        }
        static (AppCardViewModel card, IObservableFaxProcess task) Create_PdfCreatorAppCard_WithProcess(IEnvironmentState state, MultiChannelReporter reporter, string title, Action<IEnumerable<PdfJob>, Func<PdfJob, bool>> whisper)
        {
            var logger = new LogWriter(state.Dated("PdfBackLogFilePath"));
            var emailer = new EmailWriter(state, EmailBodyLambda(title, state.Dated("PdfBackLogFilePath")));
            
            var createActionsSet = PdfDrawActionsBootstrap.CreateActionsSet(state);

            var process = new DocuStreamPdfWriter(state.Dated("OutPDFFolder"), whisper, state["ValidateDataBase"], new PdfRecordProvider(state))
            {
                CreateActions = createActionsSet,
                Reporter = reporter
            };
            AppCardViewModel card = new AppCardViewModel(state, state.Dated("PdfBackLogFilePath"));

            reporter.Subscribe(card, true,      Faxback.Interface.Message.MessageWithNames(APPCARDCHANNELS));
            reporter.Subscribe(logger, true,    Faxback.Interface.Message.MessageWithNames(LOGGERCHANNELS ));
            reporter.Subscribe(emailer, false,  Faxback.Interface.Message.MessageWithNames(EMAILERCHANNELS));
            // alert log-observers.
            reporter.Update(("LOG", $"Faxback PdfCreator v{FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion}. Program startup bootstrap completed."));
            reporter.Update(("TRACKING", state["TrackingDBName"]));
            return (card, process);
        }

        static (AppCardViewModel card, IObservableFaxProcess task, Action<IEnumerable<PdfJob>, Func<PdfJob, bool>> whisper) Create_FaxbackAppCard_WithProcess (IEnvironmentState state, MultiChannelReporter reporter, string title)
        {
            if (state["FaxBackLogFilePath"] == null)
                throw new Exception("FaxBackLogFilePath was not set for an ini");
            if (state["PdfBackLogFilePath"] == null)
                throw new Exception("PdfBackLogFilePath was not set for an ini");

            var logger = new LogWriter(state.Dated("FaxBackLogFilePath"));
            var emailer = new EmailWriter(state, EmailBodyLambda(title, state.Dated("FaxBackLogFilePath")));

            var liteConfig = new FaxageLiteClientConfiguration
            {
                CompanyIdentifier = "32277",
                Password = "Docu101Stream!",
                Username = "tonylazar"
            };
            
            // Testing-mode enable.
            Func<Fax, string> faxNoResolutionStrategy = null;
            if (state["FaxTestingMode"].IsSet())
                faxNoResolutionStrategy = (_) => state["FaxTestingNumber"];
            
            var client = new FaxageLiteClient(liteConfig, faxNoResolutionStrategy);
            
            // dependent upon the IFaxClient & IFaxStream
            var process = new SimpleFaxageFaxDispatcher(client, new FaxRecordProvider(state), state["DisableOperatingThreadSleep"] == "1")
            {
                Reporter = reporter
            };

            //  /===================\                               /=============================\
            //  |  Pdf-Gen Thread:  |                               |   Fax-Send/Update Thread:   |
            //  | Do all my         |  ==========================>  | Check my buffer, which      |
            //  | work, then        |   convert pdfjob 2 faxjob     | was updated by `pdf-gen`    |
            //  | dump new records. |  ==========================>  | through this nifty channel. |
            //  \===================/                               \=============================/
            Action<IEnumerable<PdfJob>, Func<PdfJob, bool>> whisper = (buf, expr) =>
            {
                // called at the end of every Pdf-Gen thread.
                // see FaxageFax.cs for conversion here.

                lock(process.@lock) buf.ToList().ForEach(record => process.Faxes.Add(record));
            };

            AppCardViewModel card = new AppCardViewModel(state, state.Dated("FaxBackLogFilePath"));
#if DEBUG
            reporter.Subscribe(new LogWriter(@"C:\Users\ian\Desktop\" + state["TrackingDBName"] + "_TifsThatAreUpdated.log"), false, Faxback.Interface.Message.MessageWithName("DBGLOG"));
#endif
            reporter.Subscribe(card, true,      Faxback.Interface.Message.MessageWithNames(APPCARDCHANNELS));
            reporter.Subscribe(logger, true,    Faxback.Interface.Message.MessageWithNames(LOGGERCHANNELS ));
            reporter.Subscribe(emailer, false,  Faxback.Interface.Message.MessageWithNames(EMAILERCHANNELS));
            // alert log-observers.
            reporter.Update(("LOG", $"Faxback v{FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion}. Program startup bootstrap completed."));
            reporter.Update(("TRACKING", state["TrackingDBName"]));
            return (card, process, whisper);
        }

        static Func<IEnvironmentState, string, string> EmailBodyLambda(string title, string path)
        {
            return (i, m) => $"{title} encountered an error. Errors are logged in this log file: { path }";
        }

        public static List<IEnvironmentState> GetStates(List<string> inis, string title)
        {
            List<IEnvironmentState> states;
            if (inis == null)
                states = new List<IEnvironmentState> { IniEnvironmentState.ReadIni("DocuStream", title) };
            else
                states = inis.Select(path => GetDocuFaxbackIniSection(path)).ToList();
            return states;
        }

        static IEnvironmentState GetDocuFaxbackIniSection(string path)
            => new IniEnvironmentState(path, "DocuStream", new IniEnvironmentState(path, "DocuFaxback"));
    }
}