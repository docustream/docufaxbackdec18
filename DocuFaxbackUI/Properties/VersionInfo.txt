﻿// v2.0.0 -- January 24th, 2018 -- This version of Faxback took the old model and made several improvements...
//     tbd

// v2.0.1 -- January .., 2018 -- 
//  The previous version of Faxback used a NHibernateQueryableStream[T] to serve up fax and pdf entities
//   to their respective sender/creator modules.  The drawback of NHibernateQueryableStream[T] was that it
//   required frequent query (selects and updates) hits to be made against the SQL database.  In this version of Faxback, we 
//   replaced NHibernateQueryableStream[T] with __________________, which allows us to not only minimize
//   the number of query hits made, as well as parallelizing the whole application.
//
//  
//  
//  
//  