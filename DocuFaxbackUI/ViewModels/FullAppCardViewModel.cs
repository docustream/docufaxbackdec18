﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace DocuFaxbackUI.ViewModels
{
    public class FullAppCardViewModel : PropertyChangedBase
    {
        public FullAppCardViewModel(AppCardViewModel pdfCard, AppCardViewModel faxCard)
        {
            this.PdfCard = pdfCard;
            this.FaxCard = faxCard;
            AppCards = new List<AppCardViewModel> { pdfCard, faxCard };
        }

        AppCardViewModel _faxCard;
        public AppCardViewModel FaxCard {
            get => _faxCard;
            set
            {
                _faxCard = value;
                NotifyOfPropertyChange(() => FaxCard);
            }
        }

        AppCardViewModel _pdfCard;
        public AppCardViewModel PdfCard {
            get => _pdfCard;
            set
            {
                _pdfCard = value;
                NotifyOfPropertyChange(() => PdfCard);
            }
        }

        public IEnumerable<AppCardViewModel> AppCards { get; set; }
    }
}
