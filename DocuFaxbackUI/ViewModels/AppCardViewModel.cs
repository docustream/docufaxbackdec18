﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using Caliburn.Micro;
using DocuStream.ProcessingFramework.Utilities;
using Faxback.Interface;

namespace DocuFaxbackUI.ViewModels
{
    public class AppCardViewModel : PropertyChangedBase, IObserver<Faxback.Interface.Message>
    {
        private IEnvironmentState state;

        public AppCardViewModel(IEnvironmentState state, string logFilePath)
        {
            this.state = state;
            this.ShowError = Visibility.Hidden;
            this._logFilePath = logFilePath;
            this.ErrorColor = Brushes.Black;
        }

        string _trackingDatabase;
        public string TrackingDatabase
        {
            get { return this._trackingDatabase; }
            set
            {
                this._trackingDatabase = value;
                try
                { 
                    NotifyOfPropertyChange(() => TrackingDatabase);
                }
                catch { }
            }
        }

        string _batch;
        public string Batch
        {
            get { return this._batch; }
            set
            {
                this._batch = value;
                try
                {
                    NotifyOfPropertyChange(() => Batch);
                }
                catch { }
        }
        }

        string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                try
                {
                    NotifyOfPropertyChange(() => Status);
                }
                catch { }
            }
        }
        
        public bool CanOpenLogFile => true;

        public string Error { get; private set; } = "Show Log";
        Visibility _showError;
        public Visibility ShowError
        {
            get => _showError; private set
            {
                _showError = value;
                try
                {
                    NotifyOfPropertyChange(() => ShowError);
                    NotifyOfPropertyChange(() => CanOpenLogFile);
                }
                catch { }

                if (_showError == Visibility.Visible)
                {
                    ErrorColor = Brushes.Red;
                    NotifyOfPropertyChange(() => ErrorColor);
                    Error = "Errors In Log";
                    NotifyOfPropertyChange(() => Error);
                }
            }
        }
        private Brush _errorColor;
        public Brush ErrorColor
        {
            get => _errorColor;
            private set
            {
                _errorColor = value;
            }
        }

        private string _queueType;

        public string QueueType
        {
            get { return _queueType; }
            set
            {
                _queueType = value;
                try
                {
                    NotifyOfPropertyChange(() => QueueType);
                }
                catch { }
            }
        }

        private int _currentProgress = 0;

        public int CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                _currentProgress = value;
                try
                {
                    NotifyOfPropertyChange(() => CurrentProgress);
                }
                catch { }
            }
        }

        private int _maximumProgress = int.MaxValue;

        public int MaximumProgress
        {
            get { return _maximumProgress; }
            set
            {
                _maximumProgress = value;
                try
                {
                    NotifyOfPropertyChange(() => MaximumProgress);
                }
                catch { }
            }
        }
        Brush _statusColor;
        public Brush StatusColor
        {
            get
            {
                return _statusColor;
            }
            set
            {
                _statusColor = value;
                NotifyOfPropertyChange(() => StatusColor);
            }
        }


        private string _logFilePath;

        public void OpenLogFile()
        {
            System.Diagnostics.Process.Start(this._logFilePath);
        }
        
        public void Ununderline()
        {
            this._errorColor = this._lastColor;
            NotifyOfPropertyChange(() => ErrorColor);
        }

        private Brush _lastColor;

        public void Underline()
        {
            _lastColor = this._errorColor;
            this._errorColor = Brushes.Gray;
            NotifyOfPropertyChange(() => ErrorColor);
        }

        public void OnNext(Faxback.Interface.Message state)
        {
            // from the state object, we can expect to receive messages on batch or image, a status message, or a workflow message.
            switch(state.Name)
            {
                case "RUNNING":
                    this.StatusColor = Brushes.Green;
                    break;
                case "STOPPED":
                    this.StatusColor = Brushes.Red;
                    break;
                case "STATUS":
                    this.Status = state.Content;
                    break;
                case "IDENTIFIER":
                    this.Batch = state.Content;
                    break;
                case "TRACKING":
                    this.TrackingDatabase = state.Content;
                    break;
                case "ERROR":
                    this.ShowError = Visibility.Visible;
                    break;
                case "QUEUE":
                    this.CurrentProgress = int.Parse(state.Content);
                    break;
                case "QUEUE_MAX":
                    this.MaximumProgress = int.Parse(state.Content);
                    break;
                case "QUEUE_TYPE":
                    this.QueueType = state.Content;
                    break;
            }
        }

        #region unused
        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
