﻿using System;

namespace DocuFaxbackUI.ViewModels
{
    public interface IFaxbackProcessDecorator
    {
        void Stop();

        void Start();
    }
}