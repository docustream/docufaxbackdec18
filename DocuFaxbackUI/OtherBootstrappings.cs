﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using DocuStream.ProcessingFramework.Utilities;
using FaxbackModel.Implementation;
using FaxbackModel.Implementation.ORM;
using PdfSharp.Drawing;

namespace DocuFaxbackUI.Bootstraps
{
    /// <summary>
    /// Bootstrapping for the Pdf-drawing-actions. if different actions are desired, they may be composed and replaced in a static class
    /// That has a fucntion returning a CreateActions(PdfJob) delegate.
    /// </summary>
    public static class PdfDrawActionsBootstrap
    {
        public static CreateActions<PdfJob> CreateActionsSet(IEnvironmentState ini) {
            return new CreateActions<PdfJob>((PdfJob j, DataTable templates) =>
            {
                //here we can compose what kind of pdf actions we want to happen for the system.  since we have the ini readily available, we can reference it here as well!
                //very cool.
                string templatePdfPath = null;
                foreach (DataRow row in templates.Rows)
                {
                    if ((string)row["FaxbackCode"] == j.FaxbackCode)
                    {
                        templatePdfPath = (string)row["FaxFilePath"];
                        break;
                    }
                }


                // "<date/time> from Fax # <faxno>
                TextualAnnotationPdfAction textualAnnotationPdfAction = new TextualAnnotationPdfAction
                {
                    InputPath = templatePdfPath,
                    OutputPath = j.OutputPath,
                    FontSize = 16,
                    Location = new XRect(230, 80, 150, 25),
                    Message = j.Annotation
                };

                if (!string.IsNullOrEmpty(ini["PDFAnnotationTimeZone"]))
                {
                    var dates = TimeZoneInfo.GetSystemTimeZones();
                    var date    = j.Annotation.Substring(0, j.Annotation.IndexOf(" from", StringComparison.Ordinal));
                    var theRest = j.Annotation.Substring(j.Annotation.IndexOf(" from", StringComparison.Ordinal));
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.GetSystemTimeZones().First(tz => tz.StandardName == ini["PDFAnnotationTimeZone"]);
                    DateTime dateTime1 = DateTime.ParseExact(date, "MM/dd/yyyy HH:mm", null);
                    DateTime dateTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime1, destinationTimeZone);
                    textualAnnotationPdfAction.Message =
                        dateTime
                        .ToString("MM/dd/yyyy HH:mm") + theRest;
                }
                var actions = new List<PdfAction>
                          {
                            textualAnnotationPdfAction,
                            //tif image
                            new ImageAnnotationPdfAction
                            {
                                InputPath      = j.OutputPath,
                                OutputPath     = j.OutputPath,
                                ChildImagePath = j.TiffImagePath,
                                X              = (2.50 / 5.75),
                                Y              = (1.25 / 7.25),
                                Width          = (2.50 / 5.75),
                                Height         = (3.50 / 7.25)
                            }
                          };
                switch (ini["AddTiffNameToPdf"])
                {
                    case "1":
                        //add the textual annotation in the bottom corner
                        actions.Add(new TextualAnnotationPdfAction
                        {
                            InputPath = j.OutputPath,
                            OutputPath = j.OutputPath,
                            FontSize = 8,
                            Location = new XRect(90, 752, 130, 25),
                            Message = j.Identifier
                        });
                        break;
                }
                return actions;
            });
        }
    }
}
