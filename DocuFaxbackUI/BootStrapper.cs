﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Shapes;
using Caliburn.Micro;
using DocuFaxbackUI.ViewModels;
using DocuStream.ProcessingFramework.Utilities;

namespace DocuFaxbackUI
{
    public class BootStrapper : BootstrapperBase
    {
        private static readonly SimpleContainer _container = new SimpleContainer();

        public BootStrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            var inspection = ServicePointManager.SecurityProtocol;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            //ViewModelBinder.ApplyConventionsByDefault = false;
            //.AdC:\Users\ian\Documents\Visual Studio 2017\Projects\Faxback\DocuFaxbackUI\MultiflowBootstrap.cs
            _container.RegisterSingleton(typeof(IWindowManager), null, typeof(WindowManager));

            // get the program ini file.
            IniEnvironmentState ini = IniEnvironmentState.ReadIni("DocuStream");
            // if docustream section of main ini has multiflow set, pass the mainview bootstrap all the different ini's it needs to compose an             
            // appcard for.
            List<string> pathNames = ini["Multiflow"].IsSet() ?
                IniEnvironmentState.ReadIni("Multiflow").GetListProperty("Ini") :
                null;
            List<IEnvironmentState> inis = MultiflowBootstrap.GetStates(pathNames, "DocuFaxback");

            bool autostart = inis.Any(i => i["AutoStart"].IsSet());
            _container.Instance(MultiflowBootstrap.GetMainViewModel(inis, autostart, maxThreads: int.Parse(inis[0]["MaxThreads"] ?? "10")));
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;
            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }

    public static class StringExt
    {
        public static bool IsSet(this string str)
        {
            return str == "1";
        }
    }
}
