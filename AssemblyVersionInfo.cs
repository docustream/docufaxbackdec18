﻿using System.Reflection;
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
// TODO: uncomment assembly: AssemblyVersion
// Go to post-build-events and fix!

[assembly: AssemblyVersion("2.0.2.0")]
[assembly: AssemblyFileVersion("2.0.2.0")]

// Add Version History notes here. Release emails will be auto-generated based on lines that start with double-slash space Version x.x.x- MM/DD/YY where x.x.x is the current version


//
// v2.0.1 (March 1st, 2018) -- added `PDFAnnotationTimeZone` INI property, and enabled autostart.
// v2.0.1.5 (March 19th, 2018) -- enabled rejected Session updates to copy their data over from another object. this will tend to be one
//                                that has been whispered across processes.
// v2.0.1.6 (April 13th, 2018) -- changed AutoStart option functionality to be based on whether or not any DocuFaxback section contains an AutoStart=1 defintion, multiflow or otherwise.
// v2.0.1.7 (June 5th, 2018) -- changed WebClient to TLS version 1.1/1.2
// Version 2.0.1.7 -06/05/18. Description: Changed WebClient to TLS version 1.1/1.2. DocuStream received an e-mail from Faxage regarding an update to their services, which required us to support their new version. Half of the work had already been taken care of; using https://api.faxage.com/httpsfax.php from the beginning of the Faxback redesign. 
//                            However, the way we access the internet-based service depended on a .NET object which used TLS 1.0. In version 2.0.1.7, the program configures its internet access point to use TLS 1.1/1.2.
// v2.0.1.8 -12/27/2018       Resolved the issue where delivered faxes were not have their FaxStatus updated despite Faxage API indicated success
// v2.0.1.9 -12/24/2020 -- Set up INI DisableOperatingThreadSleep - tested to see if this can resolve the problem where Thread.Sleep() caused a very high CPU usage to the server that hosts the FaxBackTracking database.
//                         Updated the following functions to support DisableOperatingThreadSleep
//                         - MultiflowBootstrap.cs\Create_FaxbackAppCard_WithProcess()
//                         - SimpleFaxageDispatcher.cs\SimpleFaxageDispatcher() constructor
//          -05/20/2021 -- Updated BootStrapper.cs\Configure() line 41 to read the MaxThreads from the individual workflow INI
//                         Updated FaxageSimpleFaxDispatcher.cs constructor to exclude FaxStatus=failure from being part of unfinished faxes
// v2.0.2.0 -12/8/2021  -- Updated Faxage.com credential - DocuFaxbackUI.MultiflowBootstrap.cs\Create_PdfCreatorAppCard_WithProcess() to Tony's registered username